#!/bin/bash

err() {
    msg="$@"
    echo "ERROR:$msg" >&2
}

by_yum() {
    echo 'Found yum'
    #rpm -Uvh http://poptop.sourceforge.net/yum/stable/rhel6/pptp-release-current.noarch.rpm
    yum update -y
    yum install -y epel-release
    yum install -y pptpd
    yum install -y tmux
    config_pptpd
}

by_apt() {
    echo 'Found apt-get'
    apt-get install -y pptpd
    config_pptpd
}

config_pptpd() {
    install -b files/pptpd.conf /etc/pptpd.conf
    install -b files/pptpd-options /etc/ppp/pptpd-options
    echo "$VPN_SEC" >> files/chap-secrets
    install -b  -m 0600 files/chap-secrets /etc/ppp/chap-secrets 
    sed -ie 's/^.*net.ipv4.ip_forward.*/net.ipv4.ip_forward = 1/g' /etc/sysctl.conf
    sysctl -p
    iptables -t nat -A POSTROUTING -j SNAT -s 172.16.36.0/24 --to-source $PUB_IP
    iptables-save
}

[[ `id` == `id root` ]] || {
    err must be root to run this
}
# ------------
# MAIN
# ------------

vi cfgrc
source cfgrc

if type yum &>/dev/null; then
    by_yum
elif type apt-get &>/dev/null; then
    by_apt
else
    err 'no yum, no apt-get, no idea'
    exit 1
fi

exit 0;

