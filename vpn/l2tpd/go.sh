#!/bin/bash

OPENSWAN=openswan-2.6.38

err() {
    msg="$@"
    echo "ERROR:$msg" >&2
}

common() {
    set -x
    tar -xzf files/${OPENSWAN}.tar.gz
    ( cd $OPENSWAN; make programs install )
    for x in ipsec.conf ipsec.secrets; do
        sed -e "s/%IP_ADDRESS%/${IP_ADDRESS}/" files/${x}.template > files/$x
        install -b files/$x /etc/$x
    done
    install -b files/xl2tpd.conf /etc/xl2tpd/xl2tpd.conf
    install -b files/options.xl2tpd /etc/ppp/options.xl2tpd
    fgrep -v "$VPN_SEC" /etc/ppp/chap-secrets > files/chap-secrets
    echo "$VPN_SEC" >> files/chap-secrets
    install -b  -m 0600 files/chap-secrets /etc/ppp/chap-secrets 
    sed -i \
      -e 's/^.*\(net.ipv4.ip_forward\).*/#\1 = 1/g' \
      -e 's/^.*\(net.ipv4.conf.default.rp_filter\).*/#\1 = 0/g' \
      -e 's/^.*\(net.ipv4.conf.all.send_redirects\).*/#\1 = 0/g' \
      -e 's/^.*\(net.ipv4.conf.default.send_redirects\).*/#\1 = 0/g' \
      -e 's/^.*\(net.ipv4.conf.all.log_martians\).*/#\1 = 0/g' \
      -e 's/^.*\(net.ipv4.conf.default.log_martians\).*/#\1 = 0/g' \
      -e 's/^.*\(net.ipv4.conf.default.accept_source_route\).*/#\1 = 0/g' \
      -e 's/^.*\(net.ipv4.conf.all.accept_redirects\).*/#\1 = 0/g' \
      -e 's/^.*\(net.ipv4.conf.default.accept_redirects\).*/#\1 = 0/g' \
      -e 's/^.*\(net.ipv4.icmp_ignore_bogus_error_responses\).*/#\1 = 1/g' \
      /etc/sysctl.conf
    cat <<EOI >> /etc/sysctl.conf
net.ipv4.ip_forward = 1
net.ipv4.conf.all.accept_redirects = 0
net.ipv4.conf.all.log_martians = 0
net.ipv4.conf.all.send_redirects = 0
net.ipv4.conf.default.accept_redirects = 0
net.ipv4.conf.default.accept_source_route = 0
net.ipv4.conf.default.log_martians = 0
net.ipv4.conf.default.rpfilter = 0
net.ipv4.conf.default.send_redirects = 0
EOI
    sysctl -p
    iptables -t nat -A POSTROUTING -o $PUB_IP_IF -j MASQUERADE
    iptables-save
    set +x
}

by_yum() {
    echo 'Found yum'
    yum groupinstall -y "Development Tools"
    yum install -y gmp-devel xl2tpd lsof
    common
}


by_apt() {
    echo 'Found apt-get'
    common
}

[[ `id` == `id root` ]] || {
    err must be root to run this
}
# ------------
# MAIN
# ------------

vi cfgrc
source cfgrc

if type yum &>/dev/null; then
    by_yum
elif type apt-get &>/dev/null; then
    by_apt
else
    err 'no yum, no apt-get, no idea'
    exit 1
fi

exit 0;

