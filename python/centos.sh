#!/bin/bash

install_python2() {
    # Python 2.7.14:
    wget http://python.org/ftp/python/2.7.14/Python-2.7.14.tar.xz
    tar xf Python-2.7.14.tar.xz
    cd Python-2.7.14
    ./configure --prefix=/usr/local --enable-unicode=ucs4 --enable-shared LDFLAGS="-Wl,-rpath /usr/local/lib"
    make && make altinstall
    strip /usr/local/lib/libpython2.7.so.1.0
    python2.7 get-pip.py
}

install_python3() {
    # Python 3.6.3:
    wget http://python.org/ftp/python/3.6.3/Python-3.6.3.tar.xz
    tar xf Python-3.6.3.tar.xz
    cd Python-3.6.3
    ./configure --prefix=/usr/local --enable-shared LDFLAGS="-Wl,-rpath /usr/local/lib"
    make && make altinstall
    strip /usr/local/lib/libpython3.6m.so.1.0
    python3.6 get-pip.py
}

yum update -y

yum groupinstall -y 'development tools'

yum install -y zlib-devel bzip2-devel openssl-devel ncurses-devel sqlite-devel readline-devel tk-devel gdbm-devel db4-devel libpcap-devel xz-devel expat-devel

select pv in 2.7.14 3.6.3
do
    test -n "$pv" && break
done


case $pv in
    2.7.14)
        python2.7 --version || install_python2
        py=python2.7
        ;;
    3.6.3)
        python3.6 --version || install_python3
        py=python3.6
        ;;
esac

pip2.7 --version || {
    test -e get-pip.py || curl -LOk https://bootstrap.pypa.io/get-pip.py
    $py get-pip.py
}
